// @dart=2.9
import 'package:rootster/constants/design_system.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bootstrap/flutter_bootstrap.dart';
import 'package:rootster/pages/login_page.dart';

void main() {
  bootstrapGridParameters(
    gutterSize: 5,
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rootster',
      debugShowCheckedModeBanner: true,
      theme: ThemeData(
        primaryColor: BrandColor.Primary,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        unselectedWidgetColor: BrandColor.Grey,
        fontFamily: BrandFontFamily
      ),
      home: LoginPage(),
    );
  }
}


