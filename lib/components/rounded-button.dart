import 'package:rootster/constants/design_system.dart';
import 'package:rootster/utils/util.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final Color? foregroundColor;
  final Color? backgroundColor;
  final VoidCallback? onPressed;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final String title;
  final Widget? icon;
  final EdgeInsets? padding;
  final EdgeInsets? insets;
  final double height;
  final bool rounded;
  final TextStyle? textStyle;
  final double? maxWidth;
  final bool readOnly;

  const RoundedButton({
    Key? key,
    this.foregroundColor = BrandColor.White,
    this.backgroundColor = BrandColor.DarkGrey,
    this.onPressed,
    this.prefixIcon,
    this.suffixIcon,
    required this.title,
    this.icon,
    this.padding,
    this.insets,
    this.height = 40,
    this.rounded = false,
    this.textStyle,
    this.maxWidth,
    this.readOnly = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget widget = Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(this.rounded ? this.height / 2 : this.height / 8),
        color: (this.readOnly == false && this.onPressed == null) ? BrandColor.Grey : this.backgroundColor,
      ),
      constraints: BoxConstraints(
        maxWidth: this.maxWidth == null ? double.infinity : this.maxWidth!,
      ),
      height: this.height,
      // width: double.infinity,
      padding: this.insets,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            this.title,
            style: BrandStyler.normal.merge(
              TextStyle(
                color: this.foregroundColor
              )
            ).merge(this.textStyle),
          )
        ]
        .prepend(this.icon)
        .prepend(this.prefixIcon)
        .append(this.suffixIcon),
      ),
    );

    if (this.readOnly) {
      return Container(
        padding: this.padding,
        child: widget,
      );
    } else {
      return Container(
        padding: this.padding,
        child: InkWell(
          onTap: () {
            if (this.onPressed != null) {
              this.onPressed!();
            }
          },
          child: widget,
        ),
      );
    }
  }
}