import 'package:flutter/material.dart';
import 'package:flutter_bootstrap/flutter_bootstrap.dart';
import 'package:flutter_portal/flutter_portal.dart';
import 'package:rootster/components/footer.dart';
import 'package:rootster/components/navbar.dart';
import 'package:rootster/components/rounded-button.dart';
import 'package:rootster/constants/design_system.dart';
import 'package:rootster/utils/global_event_bus.dart';

class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Portal(
      child: Scaffold(
        appBar: Navbar.blank(),
        body: GestureDetector(
          onTap: () => {
            FocusScope.of(context).requestFocus(FocusNode()),
            GlobalEventBus.eventBus.fire(GlobalEventScreenTap())
          },
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'lib/assets/images/bg-ellipse-1.png'
                ),
                fit: BoxFit.cover,
              )
            ),
            child: ListView(
              children: [
                Container(
                  constraints: BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height - 300,
                  ),
                  padding: const EdgeInsets.only(top: 60, bottom: 60),
                  child: BootstrapContainer(
                    children: [
                      BootstrapRow(
                        children: [
                          BootstrapCol(
                            sizes: 'col-12 col-sm-10 col-md-8',
                            offsets: 'offset-0 offset-sm-1 offset-md-2',
                            child: Column(
                              children: [
                                Text(
                                  'Rootster\'s Flutter Boilerplate',
                                  textAlign: TextAlign.center,
                                  style: BrandStyler.h1.weightExtraBold,
                                ),
                                Text(
                                  'Thank you for joining us today!',
                                  textAlign: TextAlign.center,
                                  style: BrandStyler.h3.weightNormal,
                                )
                              ],
                            )
                          ),

                          BootstrapCol(
                            sizes: 'col-12 col-sm-10 col-md-8',
                            offsets: 'offset-0 offset-sm-1 offset-md-2',
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0, 40, 0, 20),
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                color: BrandColor.White,
                                boxShadow: [
                                  BoxShadow(
                                    color: BrandColor.Black.withAlpha(30),
                                    offset: Offset(0, 5),
                                    blurRadius: 10,
                                    spreadRadius: 10
                                  )
                                ],
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 30),
                                    child: Text(
                                      'congratulations!',
                                      textAlign: TextAlign.center,
                                      style: BrandStyler.normal.weightNormal.black,
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      RoundedButton(
                                        onPressed: () {
                                          Navigator.of(context).pop('Back 1');
                                        },
                                        title: 'Back 1',
                                        backgroundColor: BrandColor.Black,
                                        insets: EdgeInsets.fromLTRB(50, 5, 50, 5),
                                        textStyle: BrandStyler.normal.white,
                                        padding: EdgeInsets.only(right: 10),
                                      ),
                                      RoundedButton(
                                        onPressed: () {
                                          Navigator.of(context).pop('Back 2');
                                        },
                                        title: 'Back 2',
                                        backgroundColor: BrandColor.Black,
                                        insets: EdgeInsets.fromLTRB(50, 5, 50, 5),
                                        textStyle: BrandStyler.normal.white,
                                        padding: EdgeInsets.only(left: 10),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            )
                          )
                        ]
                      )
                    ]
                  ),
                ),

                Footer(),
              ]
            )
          )
        ),
      ),
    );
  }
}