# Rootster's Flutter Boilerplate

## About this project?
A simple Flutter project based on well-known B2B E-commerce platform [Roots Platform](http://rootsplatform.com/)

In this project, you will find some ready to use widgets / idea to create ones.

Due to it's based on the real product, there'll be no actual ___API calls___ in this project to keep the ___secret recipe___

## Demo
[Rootster](http://rootster.iamtom.co/)

## Getting started
### 1. what do you need?
- A PC based with Windows or Linux to build Android & web apps
- A Mac machine to be able to build iOS app

### 2. Install Flutter & tools
- [Flutter](https://flutter.dev/docs/get-started/install) - optional... joking! we definitely need it!
- [Enable Flutter Web Development](https://flutter.dev/docs/get-started/web) - at the time of writing this README, the web development is still experimental so we need a few steps to enabling it
- [FVM](https://fvm.app/docs/getting_started/overview) - Flutter Version Management, optional but nice to have
- Editor
  - [Visual Studio Code](https://code.visualstudio.com/) with [Flutter & Dart plugins](https://flutter.dev/docs/get-started/editor?tab=vscode) - preferred, using with this repo
  - OR [Android Studio](https://developer.android.com/studio) - do the job well if you can deal with slowness and requires lots of RAM
  - [Xcode](https://developer.apple.com/xcode/) & [Apple Developer Program](https://developer.apple.com/) - if you want to build iOS app and distributed via AppStore or TestFlight

### 3. Clone this repo
```
$ git clone git@gitlab.com:suthee-p/rootster.git
```
### 4. Build & Run on Chrome
```
$ cd /path/to/your/project
$ flutter pub get
$ flutter run -d chrome
```
#### Have fun & Happy Coding!

